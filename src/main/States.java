package main;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import states.GamePauseState;
import states.GameState;

public class States extends StateBasedGame
{   
	public static final int HEIGHT=720;
	public static final int WIDTH=(HEIGHT/9)*16;
	public static boolean debugMode=false;
	public static boolean petitMode=true;

	
	int count=0;
	int delta;
	Scanner i;
	public States(String gamename)
	{
		super(gamename);
		i=new Scanner(System.in);
		System.out.println("WIDTH>>>>> "+WIDTH);
	}

	@Override
	public void initStatesList(GameContainer arg0) throws SlickException {
		// TODO Auto-generated method stub
		this.addState(new GameState());
		this.addState(new GamePauseState());
		//this.addState(new GameOverState());
	}
	

	

	public static void main(String[] args)
	{
		try
		{
			AppGameContainer appgc;
			
			appgc = new AppGameContainer(new States("Suck Me"));
			//System.out.println(appgc.getAspectRatio());
			appgc.setDisplayMode(WIDTH, HEIGHT, false);
			appgc.setVSync(true);
			appgc.setAlwaysRender(false);
			appgc.setMouseGrabbed(true);
			appgc.start();
			//appgc.setAlwaysRender(true);
		}
		catch (SlickException ex)
		{
			Logger.getLogger(States.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
