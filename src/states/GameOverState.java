package states;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import main.States;

public class GameOverState extends BasicGameState{
	int score,goal,level;

	public GameOverState(int score, int goal, int level) {
		// TODO Auto-generated constructor stub
		this.goal=goal;
		this.score=score;
		this.level=level;
	}
	@Override
	public void init(GameContainer gc, StateBasedGame state) throws SlickException {
	System.out.println("initialised over");
	}
	@Override
	public void update(GameContainer gc, StateBasedGame state, int delta) throws SlickException {
		if(gc.getInput().isKeyDown(Input.KEY_R)){
			state.init(gc);
			state.enterState(1);
			
		}
	}
	@Override
	public void render(GameContainer gc, StateBasedGame state, Graphics g) throws SlickException {
		// TODO Auto-generated method stub
		g.drawString("Game Over    Score:"+score+" Level: "+level, 10, States.HEIGHT/2);
		
		
	}
	

	

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return 2;
	}
	@Override
	public void enter(GameContainer gc, StateBasedGame game) throws SlickException {
		gc.setMouseGrabbed(false);
	}
}

