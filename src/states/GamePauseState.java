package states;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.gui.MouseOverArea;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import main.States;

public class GamePauseState extends BasicGameState{
	@Override
	public void init(GameContainer gc, StateBasedGame state) throws SlickException {
	}
	@Override
	public void update(GameContainer gc, StateBasedGame state, int delta) throws SlickException {
		// TODO Auto-generated method stub

		if(gc.getInput().isKeyDown(Input.KEY_ESCAPE)){
			state.enterState(1, new FadeOutTransition(), new FadeInTransition());
		}
		if(gc.getInput().isKeyDown(Input.KEY_EQUALS)){
			gc.setClearEachFrame(false);
			
			
		}
		if(gc.getInput().isKeyDown(Input.KEY_DELETE)){
			
			gc.exit();
			
			
		}if(gc.getInput().isKeyDown(Input.KEY_I)){
			
			if(States.petitMode){
				States.petitMode=false;
			}else States.petitMode=true;
			
			
		}
		
		
	}
	@Override
	public void render(GameContainer gc, StateBasedGame state, Graphics g) throws SlickException {
		// TODO Auto-generated method stub
		g.drawString("Main Menu", 20, 20);
		
	}
	
	

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void enter(GameContainer gc, StateBasedGame game) throws SlickException {
		gc.setMouseGrabbed(false);
	}
}


