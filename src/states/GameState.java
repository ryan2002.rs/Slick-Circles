package states;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;
import org.newdawn.slick.state.transition.Transition;

import Objects.gameCircle;
import main.States;
import main.StopWatch;

public class GameState extends BasicGameState{
int count=0;
private ArrayList<gameCircle> balls;
private Circle mouseBall;
//sprivate StopWatch sw;
public int score;
public int lost;
public int lives;
long speed;
int goal,level;
private int elapsedTime;
	@Override
	public void init(GameContainer gc, StateBasedGame state) throws SlickException {
		// TODO Auto-generated method stub
		balls=new ArrayList<gameCircle>();
		score=0;
		lost=0;
		lives=10;
		speed=1000;
		goal=30;
		level=1;
		mouseBall=new Circle(gc.getInput().getMouseX(), gc.getInput().getMouseY(),(10f/480f)*States.HEIGHT);
	
		//sw=new StopWatch();
		//sw.start();
		
		
	}
	@Override
	public void update(GameContainer gc, StateBasedGame state, int delta) throws SlickException {
		
		for(int i=0;i<balls.size();i++){
			gameCircle c=balls.get(i);
			c.update(delta,balls);
			if(!c.exist){
				lost++;
				lives--;
				balls.remove(i);
			}
			if(c.intersects(mouseBall)&&!gc.isPaused()) {score++;balls.remove(i);}
			
			
			
		}
		if(lives==0){
			state.addState(new GameOverState(score, goal,level));
			state.enterState(2,  new FadeOutTransition(),new FadeInTransition(Color.red));
			
			
		}
		mouseBall.setCenterX(gc.getInput().getMouseX());
		mouseBall.setCenterY(gc.getInput().getMouseY());
		
		if(goal==score){
			goal+=30;
			if(!(score>=300)){
			speed-=100;
			level++;
		}
		}
		/*if(sw.getElapsedTime()>=speed){
			balls.add(new gameCircle(-20, -20, 20));
			sw.restart();	
		
		
		}*/
		if(elapsedTime >= speed) { balls.add(new gameCircle(-100, -100, 20));
		elapsedTime=0;
		 } else elapsedTime += delta; 
		if(gc.getInput().isKeyPressed(Input.KEY_P)){
			//state.enterState(0, new FadeOutTransition(), new FadeInTransition());
			if(!gc.isPaused())
			gc.pause();else gc.resume();
		}
		if(gc.getInput().isKeyDown(Input.KEY_ESCAPE)){
			if(gc.isPaused()){
				gc.resume();
			}
			state.enterState(0, new FadeOutTransition(), new FadeInTransition());
			
		}
		}

	@Override
	public void render(GameContainer gc, StateBasedGame state, Graphics g) throws SlickException {
		// TODO Auto-generated method stub
		g.setColor(Color.orange);
		g.fill(mouseBall);
		for(gameCircle c:balls){
			c.render(g);
			
		}
		if(gc.isPaused()){g.drawString("Paused", States.WIDTH/2, States.HEIGHT/2);}
		g.setColor(Color.white);
		g.drawString("Score: "+score, 10, 25);
		g.setColor(Color.red);
		g.drawString("Lost: "+lost+" Lives: "+lives, 10, 40);
		
		g.setColor(Color.green);
		g.drawString("Goal: Reach "+goal, 10, 55);
		if(level<10){
			g.drawString("Level: "+level, 10, 70);	
		}else{
			g.drawString("Level MAX", 10, 70);
		}
		g.drawString("Total balls: "+balls.size(), 10, 95);
		
		
		
		
		
	}

	

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return 1;
	}
	@Override
	public void mouseClicked(int button, int x, int y, int clickCount) {
		System.out.println("clicked x"+x+" y:"+y);
	}
	@Override
	public void enter(GameContainer gc, StateBasedGame game) throws SlickException {
		gc.setMouseGrabbed(true);
	}
	@Override
	public void keyPressed(int key, char c) {
		if(c=='p'||c=='P'){System.out.println("p pressed");}
	}
	@Override
	public void keyReleased(int key, char c) {
	}
	

	
}
